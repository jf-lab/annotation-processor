package nl.ensignprojects.annotationreporter;

import org.junit.jupiter.api.Test;
import picocli.CommandLine;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;

import static nl.ensignprojects.annotationreporter.FileProcessorTest.EXAMPLE_FILE;
import static org.assertj.core.api.Assertions.assertThat;

class MainTest {

    @Test
    void test() throws URISyntaxException {
        URL urlInput = this.getClass().getClassLoader().getResource(EXAMPLE_FILE);
        assert urlInput != null;
        var pathInput = Path.of(urlInput.toURI());

        var pathOutput = pathInput.getParent().resolve("output.csv");

        int exitCode = new CommandLine(new Main()).execute(
                "--input=" + pathInput,
                "--type=java",
                "--output=" + pathOutput
        );

        assertThat(exitCode).isEqualTo(0);
    }

    @Test
    void testWithAnnotationFilter() throws URISyntaxException {
        URL urlInput = this.getClass().getClassLoader().getResource(EXAMPLE_FILE);
        assert urlInput != null;
        var pathInput = Path.of(urlInput.toURI());

        var pathOutput = pathInput.getParent().resolve("output.csv");

        int exitCode = new CommandLine(new Main()).execute(
                "--input=" + pathInput,
                "--type=java",
                "--output=" + pathOutput,
                "--annotation-filter=Override"
        );

        assertThat(exitCode).isEqualTo(0);
    }

}