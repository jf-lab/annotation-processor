package nl.ensignprojects.annotationreporter;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

class FileProcessorTest {

    private Path testFile;

    public static final String EXAMPLE_FILE = "ExampleJavaFile.java";
    public static final String EXAMPLE_FILE_WITH_STATIC_VAR = "ExampleJavaFileWithStaticVar.java";

    private void setTestFile(String fileReference) throws URISyntaxException {
        URL url = this.getClass().getClassLoader().getResource(fileReference);
        assert url != null;
        testFile = Path.of(url.toURI());
    }

    @BeforeEach
    void setUp() throws URISyntaxException {
        setTestFile(EXAMPLE_FILE);
    }

    @Test
    @DisplayName("Annotation without arguments shows only annotation, with the values to be empty")
    void testSimpleAnnotation() {
        FileProcessor processor = new FileProcessor(testFile, "java");
        List<FindResult> results = processor.process(s -> true);

        var entries = resolveEntries(results, "Override");
        Assertions.assertThat(entries).hasSize(1);

        var entry = entries.getFirst();
        assertAll(
                () -> assertThat(entry.annotationValue()).isEmpty(),
                () -> assertThat(entry.file().getFileName().toString()).isEqualTo(EXAMPLE_FILE),
                () -> assertThat(entry.methodSignature()).isEqualTo("void test()"),
                () -> assertThat(entry.lineNumberMethod()).isEqualTo(11),
                () -> assertThat(entry.className()).isEqualTo(EXAMPLE_FILE.replace(".java", ""))
        );
    }

    @Test
    @DisplayName("Annotation with multiple values")
    void testWithMultipleValues() {
        FileProcessor processor = new FileProcessor(testFile, "java");
        List<FindResult> results = processor.process(s -> true);

        var entries = resolveEntries(results, "CsvSource");
        Assertions.assertThat(entries).hasSize(2);
        assertThat(entries.stream()
                .map(FindResult::annotationValue))
                .containsExactly("apple,         1", "banana,        2");
    }

    @Test
    @DisplayName("Annotation with single value")
    void testWithOneValue() {
        FileProcessor processor = new FileProcessor(testFile, "java");
        List<FindResult> results = processor.process(s -> true);

        var entries = resolveEntries(results, "DisplayName");
        Assertions.assertThat(entries).hasSize(1);
        assertThat(entries.getFirst().annotationValue()).isEqualTo("This is a display name");
    }

    @Test
    @DisplayName("Annotation with several named variables")
    void testWithSeveralNamedVariables() {
        FileProcessor processor = new FileProcessor(testFile, "java");
        List<FindResult> results = processor.process(s -> true);

        var entries = resolveEntries(results, "EnabledIfEnvironmentVariable");
        Assertions.assertThat(entries).hasSize(2);

        assertThat(entries.stream()
                .map(FindResult::annotationValue))
                .containsExactly("named = \"ENV\"", "matches = \"staging-server\"");
    }

    @Test
    @Disabled // TODO: implement static var support, throwing exception lead to no results at all
    @DisplayName("Test if example file contains a static variable")
    void testWithStaticVariable() throws URISyntaxException {
        setTestFile(EXAMPLE_FILE_WITH_STATIC_VAR);

        FileProcessor processor = new FileProcessor(testFile, "java");

        assertThrowsExactly(StaticVariablesNotYetSupportedException.class, () -> processor.process(s -> true));
    }

    @Test
    @DisplayName("Test if example file is not available")
    void testExampleFileIsNotAvailable() {
        FileProcessor processor = new FileProcessor(Path.of("This_FILE-does-not-exist.java"), "java");
        assertThrowsExactly(RuntimeException.class, () -> processor.process(s -> true));
    }


    private List<FindResult> resolveEntries(List<FindResult> results, String annotation) {
        return results.stream().filter(fr -> fr.annotation().equals(annotation)).toList();
    }

}