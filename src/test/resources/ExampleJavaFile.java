import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.provider.CsvSource;

public class ExampleJavaFile {

    @Deprecated
    private int bla;

    @Override
    @CsvSource({"apple,         1", "banana,        2"})
    void test() {
        System.out.println("some logic here");
    }

    @DisplayName("This is a display name")
    public int process(String name) {
        System.out.println("some logic here with @ something");
        return 0;
    }

    @EnabledIfEnvironmentVariable(named = "ENV", matches = "staging-server")
    public int processEnvironment(String name) {
        int bla = 42;
        return bla;
    }

}