package nl.ensignprojects.annotationreporter;

import java.nio.file.Path;

public record FindResult(Path file, String annotation, String annotationValue, int lineNumberMethod, String className, String methodSignature) {
    public static final String HEADER = "Full file path;Annotation;Annotation value;Line number;Class name;Method signature";
    public String toCSV() {
        return file.toString()
                + ";" + annotation
                + ";" + annotationValue
                + ";" + lineNumberMethod
                + ";" + className
                + ";" + methodSignature;
    }
}
