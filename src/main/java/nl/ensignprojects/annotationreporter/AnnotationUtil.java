package nl.ensignprojects.annotationreporter;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public interface AnnotationUtil {
    static List<FindResult> parseAnnotations(Path file, int lineNumberMethod, String className, String methodSignature, List<String> annotations) {
        return annotations.stream()
                .map(al -> {
                    String annotation;
                    String annationValue = "";
                    int parenthesisOpen = al.indexOf("(");
                    int parenthesisClosed = al.indexOf(")");

                    if (parenthesisOpen == -1) {
                        annotation = al.substring(1);
                    } else {
                        annotation = al.substring(1, parenthesisOpen);
                        String annotationArguments = al.substring(parenthesisOpen + 1, parenthesisClosed);
                        if (annotationArguments.startsWith("{") && annotationArguments.endsWith("}")) {
                            return Stream.of(al.substring(parenthesisOpen + 2, parenthesisClosed - 1).split("\","))
                                    .map(s -> s.replaceAll("\"", ""))
                                    .map(String::trim)
                                    .map(av -> new FindResult(file, annotation, av, lineNumberMethod, className, methodSignature))
                                    .toList();
                        } else if (annotationArguments.contains("=")) {
                            return Stream.of(annotationArguments.split(","))
                                    .map(String::trim)
                                    .map(av -> new FindResult(file, annotation, av, lineNumberMethod, className, methodSignature))
                                    .toList();
                        } else if (annotationArguments.startsWith("\"") && annotationArguments.endsWith("\"")) {
                            annationValue = al.substring(parenthesisOpen + 2, parenthesisClosed - 1);
                        } else {
                            System.out.println("Static variables are not supported yet: " + file + " / " + al);
                        }
                    }


                    return List.of(new FindResult(file, annotation, annationValue, lineNumberMethod, className, methodSignature));
                })
                .flatMap(Collection::stream)
                .toList();
    }
}
