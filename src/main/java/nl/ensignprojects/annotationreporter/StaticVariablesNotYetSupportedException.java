package nl.ensignprojects.annotationreporter;

public class StaticVariablesNotYetSupportedException extends RuntimeException {
    public StaticVariablesNotYetSupportedException() {
        super("Static variables are not yet supported in the annotation-reporter");
    }
}
