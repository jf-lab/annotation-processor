package nl.ensignprojects.annotationreporter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static nl.ensignprojects.annotationreporter.AnnotationUtil.parseAnnotations;

public class FileProcessor {
    private final Path file;
    private final String fileType;


    public FileProcessor(Path file, String fileType) {
        this.file = file;
        this.fileType = fileType;
    }

    public List<FindResult> process(Predicate<String> annotationPredicate) {
        var classFileName = file.toFile().getName().replace("." + fileType, "");
        var annotations = new ArrayList<String>();
        var findResults = new ArrayList<FindResult>();

        try {
            List<String> lines = Files.readAllLines(file);
            for (int i = 0; i < lines.size(); i++) {
                String line = lines.get(i);
                int lineNumber = i + 1;
                String trimmedLine = line.trim();

                if (trimmedLine.startsWith("@")) {
                    annotations.add(trimmedLine);
                }

                if (!annotations.isEmpty() && trimmedLine.endsWith("{")) {
                    String lineWithMethod = trimmedLine.replace("{", "").trim();
                    var filteredAnnotations = annotations.stream().filter(annotationPredicate).toList();
                    List<FindResult> processedAnnotations = parseAnnotations(file, lineNumber, classFileName, lineWithMethod, filteredAnnotations);
                    findResults.addAll(processedAnnotations);

                    // TODO: add logic when the curly bracket is on a new line

                    // reset annotations
                    annotations = new ArrayList<>();
                } else if (!trimmedLine.startsWith("@")) {
                    annotations = new ArrayList<>();
                }
            }
        } catch (IOException e) {
            // TODO: Add proper error handling
            throw new RuntimeException(e);
        }
        return findResults;
    }
}
