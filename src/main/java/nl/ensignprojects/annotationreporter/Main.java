package nl.ensignprojects.annotationreporter;

import picocli.CommandLine;
import picocli.CommandLine.Option;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main implements Callable<Integer> {

    @Option(names = {"-i", "--input"}, required = true, description = "Path of java project")
    private Path rootPath;

    @Option(names = {"-t", "--type"}, description = "File type to search for")
    private String fileType = "java";

    @Option(names = {"-o", "--output"}, required = true, description = "CSV output file to save the results to")
    private Path outputPath;

    @Option(names = {"-af", "--annotation-filter"}, description = "The annotation to filter")
    private String annotationFilter;

    private Predicate<String> resolvePredicateForAnnotationFilter() {
        var annotationFilterIsPresent = Optional.ofNullable(annotationFilter).isPresent();

        if (annotationFilterIsPresent) {
            var annotationToFilter = "@" + annotationFilter;
            return s -> s.startsWith(annotationToFilter);
        }

        return s -> true;
    }


    @Override
    public Integer call() {

        System.out.println("start analysis");

        try (Stream<Path> pathStream = Files.walk(rootPath)) {
            String resultInOneString = pathStream
                    .filter(p -> p.toFile().isFile())
                    .filter(p -> p.toFile().toString().endsWith(fileType))
                    .flatMap(p -> new FileProcessor(p, fileType).process(resolvePredicateForAnnotationFilter()).stream())
                    .map(FindResult::toCSV)
                    .collect(Collectors.joining("\n"));

            Files.writeString(outputPath, FindResult.HEADER + "\n" + resultInOneString);
            System.out.println("end analysis");
        } catch (Exception e) {
            System.out.println("Error occurred: " + e.getMessage());
            return 1;
        }

        return 0;
    }

    public static void main(String... args) {
        int exitCode = new CommandLine(new Main()).execute(args);
        System.exit(exitCode);
    }

}
