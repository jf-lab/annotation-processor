# annotation-reporter

The annotation-reporter lists all annotations in a class with the 
file location, so the annotations can be traced in the code.

The idea that this program can be run after the build, without loading
the project on the class path. 

In case that the line number in the code is not needed, it is better
to add a test class and get all the information with reflection.

However, if you need the line number this project can be a help. 

## how to build

Use maven in order to build the program.

```shell
mvn clean package
```

## how to run

```shell
java -jar .\target\annotation-reporter-1.0-SNAPSHOT.jar -i .\src\main\java  -o .\test.csv -af Override
```

## how to get help

```shell
java -jar .\target\annotation-reporter-1.0-SNAPSHOT.jar
```